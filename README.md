# Volume distribution indicator for Ninja Trader 7

## Download and extract
Download the [Zip-Archive](https://bitbucket.org/ninjatrader/volume-distribution/get/master.zip) and extract all ``*.cs`` files in to ``Documents\NinjaTrader 7\bin\Custom\Indicator`` directory.
