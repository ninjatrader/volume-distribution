#region Using declarations

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Xml.Serialization;
using NinjaTrader.Data;
using NinjaTrader.Gui.Chart;

#endregion Using declarations

namespace NinjaTrader.Indicator {

	[Description("Volume distribution indicator for Ninja Trader 7 by Vitalij. <vitalij [AT] gmx [DOT] de> https://bitbucket.org/ninjatrader/level2")]
	public class VolumeDistribution : Indicator {

		#region Variables

		private Brush b;
		private Brush bMax;

		private int size = 100;
		private Color colorBars = Color.Black;
		private int opacityBars = 50;

		private Color colorMaxBars = Color.Red;
		private int opacityMaxBars = 50;

		private Dictionary<DateTime, Dictionary<double, int>> dayVolumes;

		#endregion Variables

		#region Properties

		[Category("Size")]
		[NinjaTrader.Gui.Design.DisplayName("Size")]
		public int Size {
			get { return size; }
			set { size = Math.Max(1, value); }
		}

		[XmlIgnore()]
		[Category("Colors")]
		[NinjaTrader.Gui.Design.DisplayName("Color")]
		public Color ColorBars {
			get { return colorBars; }
			set { colorBars = value; }
		}

		[Category("Colors")]
		[NinjaTrader.Gui.Design.DisplayName("Opacity (%)")]
		public int OpacityBars {
			get { return opacityBars; }
			set { opacityBars = Math.Max(Math.Min(value, 100), 0); }
		}

		[XmlIgnore()]
		[Category("Colors")]
		[NinjaTrader.Gui.Design.DisplayName("Color")]
		public Color ColorMaxBars {
			get { return colorMaxBars; }
			set { colorMaxBars = value; }
		}

		[Category("Colors")]
		[NinjaTrader.Gui.Design.DisplayName("Opacity (%)")]
		public int OpacityMaxBars {
			get { return opacityMaxBars; }
			set { opacityMaxBars = Math.Max(Math.Min(value, 100), 0); }
		}

		#endregion Properties

		protected override void Initialize() {
			Overlay = true;
			AutoScale = false;
			CalculateOnBarClose = true;

			//ForceMaximumBarsLookBack256 = false;
			//MaximumBarsLookBack = MaximumBarsLookBack.Infinite;

			Add(PeriodType.Volume, size);

			dayVolumes = new Dictionary<DateTime, Dictionary<double, int>>();
			b = new SolidBrush(Color.FromArgb(opacityBars * 255 / 100, colorBars));
			bMax = new SolidBrush(Color.FromArgb(opacityMaxBars * 255 / 100, colorMaxBars));
		}

		protected override void OnBarUpdate() {
			if (BarsInProgress != 1) {
				return;
			}

			try {
				MyOnBarUpdate();
			} catch (Exception e) {
				Print(this.ToString() + ": " + e.Message);
			}
		}

		public override void Plot(Graphics graphics, Rectangle bounds, double min, double max) {
			if (BarsInProgress != 0) {
				return;
			}

			try {
				MyPlot(graphics, bounds, min, max);
			} catch (Exception e) {
				Print(this.ToString() + ": " + e.Message);
			}
		}

		protected void MyOnBarUpdate() {
			//if (CurrentBar == 0) {
			//    DrawDiamond("start-of-1-volume-data", true, 0, High[0] + TickSize * 5, Color.Pink);
			//}

			if (!dayVolumes.ContainsKey(Time[0].Date)) {
				dayVolumes.Add(Time[0].Date, new Dictionary<double, int>());
			}

			var vol = dayVolumes[Time[0].Date];

			if (!vol.ContainsKey(Close[0])) {
				vol.Add(Close[0], 0);
			}

			dayVolumes[Time[0].Date][Close[0]] += size;
		}

		public void MyPlot(Graphics graphics, Rectangle bounds, double min, double max) {
			foreach (var dayVolume in dayVolumes) {
				var mx = dayVolume.Value.Max(wert => wert.Value);

				DateTime day = dayVolume.Key;
				Dictionary<double, int> vol = dayVolume.Value;
				int volMax = vol.Values.Max();

				foreach (var kv in vol) {
					int x = ChartControl.GetXByTime(day);
					int y = ChartControl.GetYByValue(this, kv.Key);
					int width = (int)((ChartControl.GetXByTime(day.AddDays(1)) - x) / (double)volMax * kv.Value);
					int height = ChartControl.GetYByValue(this, kv.Key - TickSize) - y;

					if (kv.Value == mx) {
						graphics.FillRectangle(bMax, new Rectangle(x, y, width, height));
					} else {
						graphics.FillRectangle(b, new Rectangle(x, y, width, height));
					}
				}
			}
		}
	}
}

#region NinjaScript generated code. Neither change nor remove.

// This namespace holds all indicators and is required. Do not change it.
namespace NinjaTrader.Indicator {

	public partial class Indicator : IndicatorBase {
		private VolumeDistribution[] cacheVolumeDistribution = null;

		private static VolumeDistribution checkVolumeDistribution = new VolumeDistribution();

		/// <summary>
		/// VolumeDistribution Bands are plotted at standard deviation levels above and below a moving average. Since standard deviation is a measure of volatility, the bands are self-adjusting: widening during volatile markets and contracting during calmer periods.
		/// </summary>
		/// <returns></returns>
		public VolumeDistribution VolumeDistribution() {
			return VolumeDistribution(Input);
		}

		/// <summary>
		/// VolumeDistribution Bands are plotted at standard deviation levels above and below a moving average. Since standard deviation is a measure of volatility, the bands are self-adjusting: widening during volatile markets and contracting during calmer periods.
		/// </summary>
		/// <returns></returns>
		public VolumeDistribution VolumeDistribution(Data.IDataSeries input) {
			if (cacheVolumeDistribution != null)
				for (int idx = 0; idx < cacheVolumeDistribution.Length; idx++)
					if (cacheVolumeDistribution[idx].EqualsInput(input))
						return cacheVolumeDistribution[idx];

			lock (checkVolumeDistribution) {
				VolumeDistribution indicator = new VolumeDistribution();
				indicator.BarsRequired = BarsRequired;
				indicator.CalculateOnBarClose = CalculateOnBarClose;
#if NT7
				indicator.ForceMaximumBarsLookBack256 = ForceMaximumBarsLookBack256;
				indicator.MaximumBarsLookBack = MaximumBarsLookBack;
#endif
				indicator.Input = input;
				Indicators.Add(indicator);
				indicator.SetUp();

				VolumeDistribution[] tmp = new VolumeDistribution[cacheVolumeDistribution == null ? 1 : cacheVolumeDistribution.Length + 1];
				if (cacheVolumeDistribution != null)
					cacheVolumeDistribution.CopyTo(tmp, 0);
				tmp[tmp.Length - 1] = indicator;
				cacheVolumeDistribution = tmp;
				return indicator;
			}
		}
	}
}

// This namespace holds all market analyzer column definitions and is required. Do not change it.
namespace NinjaTrader.MarketAnalyzer {

	public partial class Column : ColumnBase {

		/// <summary>
		/// VolumeDistribution Bands are plotted at standard deviation levels above and below a moving average. Since standard deviation is a measure of volatility, the bands are self-adjusting: widening during volatile markets and contracting during calmer periods.
		/// </summary>
		/// <returns></returns>
		public Indicator.VolumeDistribution VolumeDistribution(Data.IDataSeries input) {
			return _indicator.VolumeDistribution(input);
		}
	}
}

// This namespace holds all strategies and is required. Do not change it.
namespace NinjaTrader.Strategy {

	public partial class Strategy : StrategyBase {

		/// <summary>
		/// VolumeDistribution Bands are plotted at standard deviation levels above and below a moving average. Since standard deviation is a measure of volatility, the bands are self-adjusting: widening during volatile markets and contracting during calmer periods.
		/// </summary>
		/// <returns></returns>
		public Indicator.VolumeDistribution VolumeDistribution(Data.IDataSeries input) {
			if (InInitialize && input == null)
				throw new ArgumentException("You only can access an indicator with the default input/bar series from within the 'Initialize()' method");

			return _indicator.VolumeDistribution(input);
		}
	}
}

#endregion NinjaScript generated code. Neither change nor remove.